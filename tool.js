function replace_returncode_to_comma ()
{
	var input_text = $("#replace_return_to_comma_input").val();
	$("#replace_return_to_comma_output").html(input_text.replace(/[\r\n]/g, ','));
}

function shuffle_string ()
{
	var chars = $("#shuffle_input").val().trim().split('');
	//input_text = input_text.trim();
	//chars = input_text.chars();
	let currentIndex = chars.length, randomIndex;

	// While there remain elements to shuffle.
	while (currentIndex > 0) {
		// Pick a remaining element.
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex--;
		// And swap it with the current element.
		[chars[currentIndex], chars[randomIndex]] = [chars[randomIndex], chars[currentIndex]];
	}

	$("#shuffle_output").html(chars.join(''));
}

